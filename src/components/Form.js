import React from "react";

export default class Form extends React.Component {
    render(props) {
        const { data, onChange, onSubmit } = this.props
        return (
            <form onSubmit={(event) => onSubmit(event)} className="w-75">
                <div className="form-container">
                    <label htmlFor="name" className="form-label" >Name: <span className="text-danger">{data.nameError}</span></label>
                    <input type="text" className="form-control" name="userName" id="name"
                        value={data.userName}
                        onChange={(event) => onChange(event)}
                        placeholder="Enter Full Name" />
                </div>
                <div className="form-container">
                    <label htmlFor="email" className="form-label" >Email: <span className="text-danger">{data.emailError}</span></label>
                    <input type="email" className="form-control" name="email" id="email"
                        value={data.email}
                        onChange={(event) => onChange(event)}
                        placeholder="Enter E-Mail" />
                </div>
                <div className="form-container">
                    <label htmlFor="age">Age: <span className="text-danger">{data.ageError}</span></label>
                    <input type="number" className="form-control" name="age" id="age"
                        value={data.age}
                        onChange={(event) => onChange(event)}
                        placeholder="Enter your Age" />
                </div>
                <div className="form-container">
                    <label htmlFor="pwd" className="form-label">Password: <span className="text-danger">{data.passwordError}</span></label>
                    <input type="password" className="form-control" name="password" id="pwd"
                        value={data.password}
                        onChange={(event) => onChange(event)}
                        placeholder="Enter Password" />
                </div>
                <div className="form-container">
                    <label htmlFor="confirm-pwd form-label">Confirm password: <span className="text-danger">{data.confirmPasswordError}</span></label>
                    <input type="password" className="form-control" name="confirmPassword" id="confirm-pwd"
                        value={data.confirmPassword}
                        onChange={(event) => onChange(event)}
                        placeholder="Re-enter Password" />
                </div>
                <div className="form-container">
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input"
                            id="checkbox" name="checkbox"
                            onChange={(event) => onChange(event)} checked={data.checkbox}/>
                        <label htmlFor="checkbox" className="form-check-label">I have read all Terms and Conditions</label>
                    </div>
                    <span className="text-danger">{data.checkboxError}</span>
                </div>
                <input className="btn btn-primary my-2 " type="submit" value="Sign Up" />
            </form>
        )
    }
}