import React from "react";
import Header from './Header';
import Welcome from './Welcome';
import Form from './Form';
import validator from 'validator';
import '../App.css';

export default class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            email: "",
            age: "",
            password: "",
            confirmPassword: "",
            checkbox: false,
            nameError: "",
            emailError: "",
            ageError: "",
            passwordError: "",
            confirmPasswordError: "",
            checkboxError: "",
            success: "",
        }
    }

    updateForm(event) {
        const target = event.target
        if (target.name === "checkbox") {
            this.setState({
                [target.name]: target.checked,
            })
        } else {
            this.setState({
                [target.name]: target.value,
            })
        }
    }

    checkForm({ userName, email, age, password, confirmPassword, checkbox }) {

        let flag = true;

        if (userName.length < 3 || validator.isAlpha(userName, "en-IN", { ignore: " " }) === false) {
            this.setState({
                nameError: "Name must be of three characters and only contains [a-z, A-Z, ' ']"
            })
            flag = false;
        } else {
            this.setState({
                nameError: "",
            })
        }

        if (validator.isEmail(email) === false) {
            this.setState({
                emailError: "Enter valid Email Id"
            })
            flag = false;
        } else {
            this.setState({
                emailError: "",
            })
        }

        if (age < 1 || age > 120) {
            this.setState({
                ageError: "Please, Enter Valid Age between 1 to 120"
            })
            flag = false;
        } else {
            this.setState({
                ageError: "",
            })
        }

        if (password.length <= 5) {
            this.setState({
                passwordError: "Password must be of six characters"
            })
            flag = false
        } else {
            this.setState({
                passwordError: ""
            })
        }

        if (!confirmPassword) {
            this.setState({
                confirmPasswordError: "Please enter password again!"
            })
            flag = false;
        } else if (password !== confirmPassword) {
            this.setState({
                confirmPasswordError: "Password doesn't match, try again!"
            })
            flag = false
        } else {
            this.setState({
                confirmPasswordError: ""
            })
        }

        if (!checkbox) {
            this.setState({
                checkboxError: "Please, accept the Terms and Conditions"
            })
            flag = false
        } else {
            this.setState({
                checkboxError: ""
            })
        }

        return flag;
    }

    // shouldComponentUpdate(data) {
    //     console.log("Should update");
    //     return false;
    // }

    onSubmit(event) {

        if (this.checkForm(this.state)) {
            this.setState({
                userName: "",
                email: "",
                age: "",
                password: "",
                confirmPassword: "",
                checkbox: false,
                success: "Form submitted successfully!",
            })

            setTimeout(() => {
                this.setState({
                    success: "",
                })
            }, 3000)
        }

        event.preventDefault();
    }

    render() {
        console.log("render");
        return (
            <div className="container bg-light mx-auto my-5 ">
                <div className="row">
                    <div className="col-md-6 border d-flex justify-content-center align-items-center ">
                        <Welcome />
                    </div>
                    <div className="col-md-6 border d-flex flex-column justify-content-center align-items-center p-3">
                        <Header />
                        <h3 className="text-success">{this.state.success}</h3>
                        <Form
                            data={this.state}
                            onChange={(event) => this.updateForm(event)}
                            onSubmit={(event) => this.onSubmit(event)}
                        />
                    </div>
                </div>
            </div>
        )
    }
}   