import React from "react";
import {Link} from "react-router-dom";
import '../App.css';

export default class Navbar extends React.Component {
    render() {
        return (
            <nav>
                <ul className="p-3 bg-primary d-flex justify-content-center">
                    <li className="mx-3">
                        <Link to="/" className="text-light">Home</Link>
                    </li>
                    <li className="mx-3">
                        <Link to="/about" className="text-light">About</Link>
                    </li>
                    <li className="mx-3">
                        <Link to="/signup" className="text-light">Sign Up</Link>
                    </li>
                </ul>
            </nav>
        )
    }
}